---
layout: post
title:  İnternet Site Yükleyici
date:   2019-04-14 12:00:00
tags:   Programlama
categories: [Programlama ]
comments: false
published: true
lang: tr
---


HTTrack is a free (GPL, libre/free software) and easy-to-use offline browser utility.

It allows you to download a World Wide Web site from the Internet to a local directory, building recursively all directories, getting HTML, images, and other files from the server to your computer. HTTrack arranges the original site's relative link-structure. Simply open a page of the "mirrored" website in your browser, and you can browse the site from link to link, as if you were viewing it online. HTTrack can also update an existing mirrored site, and resume interrupted downloads. HTTrack is fully configurable, and has an integrated help system.

git clone https://github.com/xroche/httrack.git --recurse
cd httrack
./configure && make -j8 && make install DESTDIR=/

*wget -q -O - "http://wordpress.org/latest.tar.gz" | tar -xzf - -C /var/www*

<br>
<h2>Summary of all useful commands</h2><table border="1"><th colspan="1" rowspan="1" align="center" valign="middle" style="background-color: #ffdead; color: black; ">Description</th><th colspan="1" rowspan="1" align="center" valign="middle" style="background-color: #ffdead; color: black; ">Command</th><tr><td>To see which jobs are still running jobs</td><td><kbd>jobs</kbd><br /><kbd>jobs -l</kbd><br /><kbd>ps aux</kbd></td></tr><tr><td>To put a command / script to the background</td><td><kbd>command &</kbd><br /><kbd>/path/to/command &</kbd><br /><kbd>/path/to/script arg1 &</kbd></td></tr><tr><td>To bring a background job to the foreground</td><td><kbd>fg n</kbd><br /><kbd>%n</kbd></td></tr><tr><td>To send a job to the background without canceling it</td><td><kbd>bg n</kbd><br /><kbd>%n &</kbd></td></tr></table><p><small>Note: n == Job id (use <kbd>jobs</kbd> command to see job id).</small>.</p><h5>See also:</h5><ul><li><a href="https://bash.cyberciti.biz/guide/Putting_jobs_in_background">Putting jobs in background</a> from the Linux shell scripting tutorial.</li><li>Command examples pages: <a href="https://www.cyberciti.biz/faq/unix-linux-jobs-command-examples-usage-syntax/" title="See Linux/Unix jobs command examples for more info">jobs command</a>, <a href="https://www.cyberciti.biz/faq/unix-linux-bg-command-examples-usage-syntax/" title="See Linux/Unix bg command examples for more info">bg command</a>, and <a href="https://www.cyberciti.biz/faq/unix-linux-fg-command-examples-usage-syntax/" title="See Linux/Unix fg command examples for more info">fg command</a></li><li>Man pages: ksh(1)</li></ul>

'''
#import
from InstagramAPI import InstagramAPI # Instagram API -> https://github.com/LevPasha/Instagram-API-python
from random import randint # random number for random picture
import schedule # Schedule -> https://github.com/dbader/schedule
from dataclasses import dataclass
import time
import os

#login
usr = "zekidemir800" # username
pwd = ""    # password
API = InstagramAPI(usr,pwd)
API.login() # login

#caption
caption = "•(8)They want to extinguish Allah's light with their mouths, and Allah will complete his light, even if the disbelievers dislike it.•61 Saff•••#instacool #insta #instaphoto #instago #instasize #instapassport #visiting #igtravel #tourist #instatraveling #mytravelgram #tourism #instafollow #instamoment #tweegram #webstagram #셀피 #셀카 #selca #셀스타그램 #얼스타그램 #look #all_shots #iphoneonly #선팔 #20likes #인스타그램 #좋아요 #instachile #팔로우 "

def random_picture(): # we require you to rename your picture as number :3
     pic = randint(1,680) # random number 1 - 3
     rdm = randint(1,2) # random number 1 - 3
     auto_post(pic,rdm)

def auto_post(pic,rdm):
    picname = str(pic)+".jpg" # picture name
    strcon = r'/picture'+str(rdm) # concat picture directory
    def_path = os.getcwd()+strcon # get current directory
    photo_path = def_path+"/"+picname
    API.uploadPhoto(photo_path, caption)

#schedule.every(159).minutes.do(random_picture) # every ? minutes
schedule.every(3).hours.do(random_picture) # every ? hours

while True:
    schedule.run_pending() # waiting for schedule
    time.sleep(9) # countdown 1 second

'''

<!-- toggle mode -->
<div>
   <i class="fa fa-sun-o fa-1x" aria-hidden="true"></i>&nbsp;<label class="theme-switch">
   <input type="checkbox" id="switch-style" data-toggle="toggle">
   <div class="slider"></div>
   </label>&nbsp;<i class="fa fa-moon-o fa-1x" aria-hidden="true"></i>
 
</div>
<!-- toggle  CSS -->
<link rel="stylesheet" href="{{ site.baseurl }}/css/toggle.css" type="text/css" >

<!-- toggle  jS -->
<script defer src="{{ site.baseurl }}/scripts/toggle.js"></script>

