---
layout: post
title:  Pisi Linux Siyah-Beyaz Çıkartmalar Hazır
date:   2015-09-25 15:00:00
tags:   Pisi Linux Etiket
categories: [Pisi Linux]
comments: false
published: true
lang: tr
---

Pisi Linux tanıtımı için tasarlanan siyah-beyaz çıkartma örnekleri GIMP kullanılarak hazırlandı. 

***Snapshots***

![ ]({{ site.baseurl }}/assets/Pisi-Linux-600x600-1.png)
![ ]({{ site.baseurl }}/assets/Pisi-Linux-600x600-2.png)
![ ]({{ site.baseurl }}/assets/Pisi-Linux-600x600-3.png)



{% include dist.html %}
 
